package trader;

import model.Model;
import model.Oggetto;

import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXSlider;
import com.jfoenix.controls.JFXTextField;
import java.net.URL;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import java.util.*;

public class TraderFXMLController {
    
    
    /* PRIVATO */
    private int sliderValue = 1;
    
    private Model model; 
    private Tooltip tt = new Tooltip("1 - acquisto x1. \n2 - x10,\n3-x100");
    private List<Oggetto> listaOggetto;
    
    private List<ImageView> contenitoreImmaginiMercato;
    private List<JFXTextField> contenitoreFieldNomeOggettoMercato;
    private List<JFXTextField> contenitoreFieldPrezzoOggettoMercato;
    private List<JFXTextField> contenitoreFieldQntOggettoMercato;

    
    private List<Oggetto> listaMercato; // 10 elementi
    
    public void setModel(Model model) {
	this.model = model;
    }
    
    private int getSliderValue() {
	if (slider.getValue() == 1) {
	    this.sliderValue = 1;
	} else if (slider.getValue() == 2) {
	    this.sliderValue = 10;
	} else {
	    this.sliderValue = 100;
	}
	return sliderValue;
    }
    
    private void aggiornaListaLocaleOggetti(List<Oggetto> lista) {
	listaOggetto = lista;
    	for (Oggetto o : listaOggetto) {
    	    System.out.println(o);
    	}
    }
    
    private void inizializzaContenitori() {
	contenitoreImmaginiMercato = new ArrayList<ImageView>(10);
	contenitoreImmaginiMercato.add(0, img1);
	contenitoreImmaginiMercato.add(1, img2);
	contenitoreImmaginiMercato.add(2, img3);
	contenitoreImmaginiMercato.add(3, img4);
	contenitoreImmaginiMercato.add(4, img5);
	contenitoreImmaginiMercato.add(5, img6);
	contenitoreImmaginiMercato.add(6, img7);
	contenitoreImmaginiMercato.add(7, img8);
	contenitoreImmaginiMercato.add(8, img9);
	contenitoreImmaginiMercato.add(9, img10);
	
	contenitoreFieldNomeOggettoMercato = new ArrayList<JFXTextField>(10);
	contenitoreFieldNomeOggettoMercato.add(0, nomeOggetto1);
	contenitoreFieldNomeOggettoMercato.add(1, nomeOggetto2);
	contenitoreFieldNomeOggettoMercato.add(2, nomeOggetto3);
	contenitoreFieldNomeOggettoMercato.add(3, nomeOggetto4);
	contenitoreFieldNomeOggettoMercato.add(4, nomeOggetto5);
	contenitoreFieldNomeOggettoMercato.add(5, nomeOggetto6);
	contenitoreFieldNomeOggettoMercato.add(6, nomeOggetto7);
	contenitoreFieldNomeOggettoMercato.add(7, nomeOggetto8);
	contenitoreFieldNomeOggettoMercato.add(8, nomeOggetto9);
	contenitoreFieldNomeOggettoMercato.add(9, nomeOggetto10);
	
	contenitoreFieldPrezzoOggettoMercato = new ArrayList<JFXTextField>(10);
	contenitoreFieldPrezzoOggettoMercato.add(0, prezzo1);
	contenitoreFieldPrezzoOggettoMercato.add(1, prezzo2);
	contenitoreFieldPrezzoOggettoMercato.add(2, prezzo3);
	contenitoreFieldPrezzoOggettoMercato.add(3, prezzo4);
	contenitoreFieldPrezzoOggettoMercato.add(4, prezzo5);
	contenitoreFieldPrezzoOggettoMercato.add(5, prezzo6);
	contenitoreFieldPrezzoOggettoMercato.add(6, prezzo7);
	contenitoreFieldPrezzoOggettoMercato.add(7, prezzo8);
	contenitoreFieldPrezzoOggettoMercato.add(8, prezzo9);
	contenitoreFieldPrezzoOggettoMercato.add(9, prezzo10);
	
	contenitoreFieldQntOggettoMercato = new ArrayList<JFXTextField>(10);
	contenitoreFieldQntOggettoMercato.add(0, qnt1);
	contenitoreFieldQntOggettoMercato.add(1, qnt2);
	contenitoreFieldQntOggettoMercato.add(2, qnt3);
	contenitoreFieldQntOggettoMercato.add(3, qnt4);
	contenitoreFieldQntOggettoMercato.add(4, qnt5);
	contenitoreFieldQntOggettoMercato.add(5, qnt6);
	contenitoreFieldQntOggettoMercato.add(6, qnt7);
	contenitoreFieldQntOggettoMercato.add(7, qnt8);
	contenitoreFieldQntOggettoMercato.add(8, qnt9);
	contenitoreFieldQntOggettoMercato.add(9, qnt10);
	
    }
    
    
    
    /* PRIVATO */
	
    @FXML // ResourceBundle that was given to the FXMLLoader
    private ResourceBundle resources;

    @FXML // URL location of the FXML file that was given to the FXMLLoader
    private URL location;

    @FXML // fx:id="btnCompra"
    private JFXButton btnCompra; // Value injected by FXMLLoader

    @FXML // fx:id="btnVendi"
    private JFXButton btnVendi; // Value injected by FXMLLoader

    @FXML // fx:id="btnZaino"
    private JFXButton btnZaino; // Value injected by FXMLLoader

    @FXML // fx:id="principalePane"
    private AnchorPane principalePane; // Value injected by FXMLLoader

    @FXML // fx:id="nomeField"
    private JFXTextField nomeField; // Value injected by FXMLLoader

    @FXML // fx:id="cognomeFIeld"
    private JFXTextField cognomeFIeld; // Value injected by FXMLLoader

    @FXML // fx:id="etaField"
    private JFXTextField etaField; // Value injected by FXMLLoader

    @FXML // fx:id="btnIniziamo"
    private JFXButton btnIniziamo; // Value injected by FXMLLoader

    @FXML // fx:id="compraPane"
    private AnchorPane compraPane; // Value injected by FXMLLoader

    @FXML // fx:id="img1"
    private ImageView img1; // Value injected by FXMLLoader

    @FXML // fx:id="img2"
    private ImageView img2; // Value injected by FXMLLoader

    @FXML // fx:id="img3"
    private ImageView img3; // Value injected by FXMLLoader

    @FXML // fx:id="img4"
    private ImageView img4; // Value injected by FXMLLoader

    @FXML // fx:id="img5"
    private ImageView img5; // Value injected by FXMLLoader

    @FXML // fx:id="nomeOggetto1"
    private JFXTextField nomeOggetto1; // Value injected by FXMLLoader

    @FXML // fx:id="nomeOggetto2"
    private JFXTextField nomeOggetto2; // Value injected by FXMLLoader

    @FXML // fx:id="nomeOggetto3"
    private JFXTextField nomeOggetto3; // Value injected by FXMLLoader

    @FXML // fx:id="nomeOggetto4"
    private JFXTextField nomeOggetto4; // Value injected by FXMLLoader

    @FXML // fx:id="nomeOggetto5"
    private JFXTextField nomeOggetto5; // Value injected by FXMLLoader

    @FXML // fx:id="img6"
    private ImageView img6; // Value injected by FXMLLoader

    @FXML // fx:id="img7"
    private ImageView img7; // Value injected by FXMLLoader

    @FXML // fx:id="img8"
    private ImageView img8; // Value injected by FXMLLoader

    @FXML // fx:id="img9"
    private ImageView img9; // Value injected by FXMLLoader

    @FXML // fx:id="img10"
    private ImageView img10; // Value injected by FXMLLoader

    @FXML // fx:id="nomeOggetto6"
    private JFXTextField nomeOggetto6; // Value injected by FXMLLoader

    @FXML // fx:id="nomeOggetto7"
    private JFXTextField nomeOggetto7; // Value injected by FXMLLoader

    @FXML // fx:id="nomeOggetto8"
    private JFXTextField nomeOggetto8; // Value injected by FXMLLoader

    @FXML // fx:id="nomeOggetto9"
    private JFXTextField nomeOggetto9; // Value injected by FXMLLoader

    @FXML // fx:id="nomeOggetto10"
    private JFXTextField nomeOggetto10; // Value injected by FXMLLoader

    @FXML // fx:id="prezzo1"
    private JFXTextField prezzo1; // Value injected by FXMLLoader

    @FXML // fx:id="qnt1"
    private JFXTextField qnt1; // Value injected by FXMLLoader

    @FXML // fx:id="prezzo2"
    private JFXTextField prezzo2; // Value injected by FXMLLoader

    @FXML // fx:id="prezzo3"
    private JFXTextField prezzo3; // Value injected by FXMLLoader

    @FXML // fx:id="prezzo4"
    private JFXTextField prezzo4; // Value injected by FXMLLoader

    @FXML // fx:id="prezzo5"
    private JFXTextField prezzo5; // Value injected by FXMLLoader

    @FXML // fx:id="qnt2"
    private JFXTextField qnt2; // Value injected by FXMLLoader

    @FXML // fx:id="qnt3"
    private JFXTextField qnt3; // Value injected by FXMLLoader

    @FXML // fx:id="qnt4"
    private JFXTextField qnt4; // Value injected by FXMLLoader

    @FXML // fx:id="qnt5"
    private JFXTextField qnt5; // Value injected by FXMLLoader

    @FXML // fx:id="prezzo6"
    private JFXTextField prezzo6; // Value injected by FXMLLoader

    @FXML // fx:id="qnt6"
    private JFXTextField qnt6; // Value injected by FXMLLoader

    @FXML // fx:id="prezzo7"
    private JFXTextField prezzo7; // Value injected by FXMLLoader

    @FXML // fx:id="prezzo8"
    private JFXTextField prezzo8; // Value injected by FXMLLoader

    @FXML // fx:id="prezzo9"
    private JFXTextField prezzo9; // Value injected by FXMLLoader

    @FXML // fx:id="prezzo10"
    private JFXTextField prezzo10; // Value injected by FXMLLoader

    @FXML // fx:id="qnt7"
    private JFXTextField qnt7; // Value injected by FXMLLoader

    @FXML // fx:id="qnt8"
    private JFXTextField qnt8; // Value injected by FXMLLoader

    @FXML // fx:id="qnt9"
    private JFXTextField qnt9; // Value injected by FXMLLoader

    @FXML // fx:id="qnt10"
    private JFXTextField qnt10; // Value injected by FXMLLoader

    @FXML // fx:id="vendiPane"
    private AnchorPane vendiPane; // Value injected by FXMLLoader

    @FXML // fx:id="btnAggiungiListaVendita"
    private JFXButton btnAggiungiListaVendita; // Value injected by FXMLLoader

    @FXML // fx:id="slider"
    private JFXSlider slider; // Value injected by FXMLLoader
    
    @FXML
    void doIniziamo(ActionEvent event) {
	if (nomeField.getText() != null && cognomeFIeld.getText()  != null && etaField.getText()  != null) {
            btnVendi.setDisable(false);
            btnCompra.setDisable(false);
	    btnZaino.setDisable(false);
	    principalePane.setVisible(false);
 
	    aggiornaListaLocaleOggetti(model.getListAllObj());
	    
	    doCompra(event);
	    
	}
    }
    

    @FXML
    void doSliderTooltip(MouseEvent event) {
	if (!tt.isActivated()) {
        	tt.show(slider, event.getScreenX()-80, event.getScreenY()-80);
        	tt.setAutoHide(true);
	}
    }
    
    @FXML
    void doSliderTooltipEXT(MouseEvent event) {
	

    }
    
    
    
    @FXML
    void doAcquista1(MouseEvent event) {
	getSliderValue();
	tt = new Tooltip();
	tt.setText("Hai comprato " + sliderValue + nomeOggetto1.getText());
	tt.show(img1, img1.getX(), img1.getY());
	model.acquistaOggetto(nomeOggetto1.getText(), sliderValue);

    }
    
    @FXML
    void doAcquista2(MouseEvent event) {

    }

    @FXML
    void doAcquista3(MouseEvent event) {

    }

    @FXML
    void doAcquista4(MouseEvent event) {

    }

    @FXML
    void doAcquista5(MouseEvent event) {

    }

    @FXML
    void doAcquista6(MouseEvent event) {

    }

    @FXML
    void doAcquista7(MouseEvent event) {

    }

    @FXML
    void doAcquista8(MouseEvent event) {

    }

    @FXML
    void doAcquista9(MouseEvent event) {

    }
    
    @FXML
    void doAcquista10(MouseEvent event) {

    }

    @FXML
    void doCompra(ActionEvent event) {
	compraPane.setVisible(true);
	
	//AGGIORNA TUTTI I COMPONENTI DEL MERCATO
	
	
	img1.setImage(new Image(model.trovaSingoloOggetto("Ferro").getImmagine()));
	nomeOggetto1.setText(model.trovaSingoloOggetto("Ferro").getNomeOggetto());
	prezzo1.setText(String.valueOf(model.trovaSingoloOggetto("Ferro").getPrezzo()));
	qnt1.setText(String.valueOf(model.trovaSingoloOggetto("Ferro").getQnt()));
	// scarica dal db se non � gia stato fatto la corrente lista di oggetti
	
    }
    
    private void aggiornaListeUI() {

	
    }


    @FXML
    void doVendi(ActionEvent event) {

    }

    @FXML
    void doZaino(ActionEvent event) {

    }

    
    
    
    
    
    
    
    
    
    @FXML // This method is called by the FXMLLoader when initialization is complete
    void initialize() {
        assert btnCompra != null : "fx:id=\"btnCompra\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert btnVendi != null : "fx:id=\"btnVendi\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert btnZaino != null : "fx:id=\"btnZaino\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert principalePane != null : "fx:id=\"principalePane\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert nomeField != null : "fx:id=\"nomeField\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert cognomeFIeld != null : "fx:id=\"cognomeFIeld\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert etaField != null : "fx:id=\"etaField\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert btnIniziamo != null : "fx:id=\"btnIniziamo\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert compraPane != null : "fx:id=\"compraPane\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert img1 != null : "fx:id=\"img1\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert img2 != null : "fx:id=\"img2\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert img3 != null : "fx:id=\"img3\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert img4 != null : "fx:id=\"img4\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert img5 != null : "fx:id=\"img5\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert nomeOggetto1 != null : "fx:id=\"nomeOggetto1\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert nomeOggetto2 != null : "fx:id=\"nomeOggetto2\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert nomeOggetto3 != null : "fx:id=\"nomeOggetto3\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert nomeOggetto4 != null : "fx:id=\"nomeOggetto4\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert nomeOggetto5 != null : "fx:id=\"nomeOggetto5\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert img6 != null : "fx:id=\"img6\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert img7 != null : "fx:id=\"img7\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert img8 != null : "fx:id=\"img8\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert img9 != null : "fx:id=\"img9\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert img10 != null : "fx:id=\"img10\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert nomeOggetto6 != null : "fx:id=\"nomeOggetto6\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert nomeOggetto7 != null : "fx:id=\"nomeOggetto7\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert nomeOggetto8 != null : "fx:id=\"nomeOggetto8\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert nomeOggetto9 != null : "fx:id=\"nomeOggetto9\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert nomeOggetto10 != null : "fx:id=\"nomeOggetto10\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert prezzo1 != null : "fx:id=\"prezzo1\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert qnt1 != null : "fx:id=\"qnt1\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert prezzo2 != null : "fx:id=\"prezzo2\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert prezzo3 != null : "fx:id=\"prezzo3\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert prezzo4 != null : "fx:id=\"prezzo4\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert prezzo5 != null : "fx:id=\"prezzo5\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert qnt2 != null : "fx:id=\"qnt2\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert qnt3 != null : "fx:id=\"qnt3\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert qnt4 != null : "fx:id=\"qnt4\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert qnt5 != null : "fx:id=\"qnt5\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert prezzo6 != null : "fx:id=\"prezzo6\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert qnt6 != null : "fx:id=\"qnt6\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert prezzo7 != null : "fx:id=\"prezzo7\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert prezzo8 != null : "fx:id=\"prezzo8\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert prezzo9 != null : "fx:id=\"prezzo9\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert prezzo10 != null : "fx:id=\"prezzo10\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert qnt7 != null : "fx:id=\"qnt7\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert qnt8 != null : "fx:id=\"qnt8\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert qnt9 != null : "fx:id=\"qnt9\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert qnt10 != null : "fx:id=\"qnt10\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert vendiPane != null : "fx:id=\"vendiPane\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert btnAggiungiListaVendita != null : "fx:id=\"btnAggiungiListaVendita\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        assert slider != null : "fx:id=\"slider\" was not injected: check your FXML file 'TraderFXML.fxml'.";
        
        compraPane.setVisible(false);
        vendiPane.setVisible(false);
        principalePane.setVisible(true);
        

    }
}
