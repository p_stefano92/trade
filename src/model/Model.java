package model;

import java.sql.*;
import java.util.*;


public class Model {
    
    private List<Oggetto> listOggetti;
    private DBConnect db = new DBConnect();

    
    public Model() {
	this.listOggetti = new ArrayList<Oggetto>();

    }
    
    public Oggetto trovaSingoloOggetto(String nomeOggetto) {
        db.createConnection();
        return db.findOggetto(nomeOggetto);
    }
    
    public List<Oggetto> getListAllObj() {
	db.createConnection();
	return db.listOggetti();
    }
    
    public Oggetto acquistaOggetto(String nomeOggetto, int qnt) {
	db.createConnection();
	return db.acquisto(nomeOggetto, qnt);
    }
    
    public void addList(Oggetto o) {
        if (!listOggetti.contains(o)) {
            listOggetti.add(o);
        }
    }

    @Override
    public String toString() {
        return "Model{" + "Oggetto = " + listOggetti.toString() + '}';
    } 
    







    private class DBConnect { 
	
	private List<Oggetto> index = new ArrayList<Oggetto>();
	
        private final String url = "jdbc:mysql://localhost:3306/trade?user=root&password=";
        private Connection con;
        private ResultSet res;
        private PreparedStatement cmd; 
        
        private void createConnection() {
            try {

                Class.forName("com.mysql.jdbc.Driver");
                con = DriverManager.getConnection(url);                
                
            } catch (SQLException | ClassNotFoundException e) {
                e.printStackTrace();
            } 
        }
                    
            
        public Oggetto findOggetto(String nomeOggetto) {
// CONTROLLO SE L'INDEX HA GIA l?ELEMENTO
            try {
		
		String qry = "SELECT * FROM oggetto WHERE nomeOggetto=?";
		cmd = con.prepareStatement(qry);
                cmd.setString(1, nomeOggetto);
                res = cmd.executeQuery();
                Oggetto result = null;
            while (res.next()) {
        	result = new Oggetto(res.getString("percorsoImg"), res.getString("nomeOggetto"), res.getInt("prezzo"), res.getInt("qnt"));
            }   
                      
            res.close();
            cmd.close();
            con.close();
            
            return result;
	    } catch (SQLException e) {
		try {
		    cmd.close();
		    con.close();
		} catch (SQLException e1) {
		    e1.printStackTrace();
		}

		e.printStackTrace();
		return null;
	    }
	    
        }
        
        



	public List<Oggetto> listOggetti() {
            boolean listOk = false;   
            int i = 1;
            
            while (!listOk) {
        	Oggetto result = null;
        	// richiesta al db oggetti 
        	try {
        	    String qry = "SELECT * FROM oggetto WHERE id=?";
        	    cmd = con.prepareStatement(qry);
        	    cmd.setInt(1, i);
        	    i++;
        	    res = cmd.executeQuery();
                    
        	    while (res.next()) {
                        result = new Oggetto(res.getString("percorsoImg"), res.getString("nomeOggetto"), res.getInt("prezzo"), res.getInt("qnt"));
                        if (!index.contains(result) ) {
                            index.add(result);
                        }
        	    }
        	    if (result == null) {
        		listOk = true;
        	    }

         	} catch (SQLException e) {
        	    try {
                        res.close();
                        cmd.close();
                        con.close();
        	    } catch (SQLException ex) {
        		return index;
        	    }
        	    
        	    
        	}

            }
	    return index;
        }
	
	/**
	 * Rimuove "qnt" dal db e lo assegna al player
	 * @param nomeOggetto oggetto che si vuole acquistare
	 * @param qnt la quantita che si vuole acquistare
	 * @return un oggetto con tot qnt
	 */
	public Oggetto acquisto(String nomeOggetto, int qnt) {
	    try {
		String qry0 = "SELECT * FROM oggetto WHERE nomeOggetto=?";
		String qry = "UPDATE oggetto SET qnt = qnt - ? where nomeOggetto ='" + nomeOggetto + "'";
		Oggetto result = null;
		
		cmd = con.prepareStatement(qry0);
		cmd.setString(1, nomeOggetto);
                res = cmd.executeQuery();
               
                while (res.next()) {
                    result = new Oggetto(res.getString("percorsoImg"), res.getString("nomeOggetto"), res.getInt("prezzo"), res.getInt("qnt"));
                }
                
                // se non c'� aggiungilo all'index gia con la quantita in meno altrimenti aggiorna
                result.setQnt(result.getQnt() - qnt);
                if (!index.contains(result)) { 
                    index.add(result);
                } else {
                    index.get(index.indexOf(result)).setQnt(result.getQnt() - qnt );
                }
                cmd.clearBatch();
                cmd = con.prepareStatement(qry);    
                if (!res.wasNull()) {
                    cmd.setInt(1, qnt);
                    cmd.executeUpdate();
                }
                
                      
            res.close();
            cmd.close();
            con.close();
            
            return result;
	    } catch (SQLException e) {
		try {
		    cmd.close();
		    con.close();
		} catch (SQLException e1) {
		    e1.printStackTrace();
		}

		e.printStackTrace();
		return null;
	    }
        }


	@Override
	public int hashCode() {
	    final int prime = 31;
	    int result = 1;
	    result = prime * result + getOuterType().hashCode();
	    result = prime * result + ((index == null) ? 0 : index.hashCode());
	    return result;
	}


	@Override
	public boolean equals(Object obj) {
	    if (this == obj)
		return true;
	    if (obj == null)
		return false;
	    if (getClass() != obj.getClass())
		return false;
	    DBConnect other = (DBConnect) obj;
	    if (!getOuterType().equals(other.getOuterType()))
		return false;
	    if (index == null) {
		if (other.index != null)
		    return false;
	    } else if (!index.equals(other.index))
		return false;
	    return true;
	}


	private Model getOuterType() {
	    return Model.this;
	}
	
	
	
	
	
	
    }
}
    
    

