/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Stefano
 */
public class TestModel {
    
    public static void main(String[] args) {
        
        Model model = new Model();
        
        
        
        List<Oggetto> list = model.getListAllObj();
        for (Oggetto o : list) {
            System.out.println(o.toString());
        }
        
        
        System.out.print("\n" + model.trovaSingoloOggetto("Ferro").toString());
        
        
        Oggetto acquisto = model.acquistaOggetto("Rame", 10);
        
        System.out.println("acquistato: " + acquisto.getNomeOggetto() + " rimanente: " + acquisto.getQnt());
    }
    
}
