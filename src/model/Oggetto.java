package model;

public class Oggetto {
    
    private String immagine; // percorso immagine
    private String nomeOggetto;
    private int prezzo;
    private int qnt;   
    
    public Oggetto(String image, String nomeOggetto, int prezzo, int qnt) {
	this.immagine = image;
	this.nomeOggetto = nomeOggetto;
	this.prezzo = prezzo;
	this.qnt = qnt;
    }
    
    public String getImmagine() {
        return immagine;
    }

    public void setImmagine(String immagine) {
        this.immagine = immagine;
    }

    public String getNomeOggetto() {
        return nomeOggetto;
    }

    public void setNomeOggetto(String nomeOggetto) {
        this.nomeOggetto = nomeOggetto;
    }

    public int getPrezzo() {
        return prezzo;
    }

    public void setPrezzo(int prezzo) {
        this.prezzo = prezzo;
    }

    public int getQnt() {
        return qnt;
    }

    public void setQnt(int qnt) {
        this.qnt = qnt;
    }

    @Override
    public String toString() {
	return "Oggetto [nomeOggetto=" + nomeOggetto + ", prezzo=" + prezzo + ", qnt=" + qnt + "]";
    }

    @Override
    public int hashCode() {
	final int prime = 31;
	int result = 1;
	result = prime * result + ((immagine == null) ? 0 : immagine.hashCode());
	result = prime * result + ((nomeOggetto == null) ? 0 : nomeOggetto.hashCode());
	result = prime * result + prezzo;
	result = prime * result + qnt;
	return result;
    }

    @Override
    public boolean equals(Object obj) {
	if (this == obj)
	    return true;
	if (obj == null)
	    return false;
	if (getClass() != obj.getClass())
	    return false;
	Oggetto other = (Oggetto) obj;
	if (immagine == null) {
	    if (other.immagine != null)
		return false;
	} else if (!immagine.equals(other.immagine))
	    return false;
	if (nomeOggetto == null) {
	    if (other.nomeOggetto != null)
		return false;
	} else if (!nomeOggetto.equals(other.nomeOggetto))
	    return false;
	if (prezzo != other.prezzo)
	    return false;
	if (qnt != other.qnt)
	    return false;
	return true;
    }

    

    
    
    
}
